import { describe, test, expect } from 'vitest';
import { Parser } from '../Parser';

describe('Test suite for variable assignment expressions', () => {

  const parser = new Parser();

  test('should parse an assignment expression', () => {
    const program = 'x = 42;';

    expect(parser.parse(program)).toStrictEqual({
      type: 'Program',
      body: [
        {
          type: 'ExpressionStatement',
          expression: {
            type: 'AssignmentExpression',
            operator: '=',
            left: {
              type: 'Identifier',
              name: 'x'
            },
            right: {
              type: 'NumericLiteral',
              value: 42
            }
          }
        }
      ]
    });
  });

  test('should parse multiple assignments', () => {
    const program = 'x = y = 42;';

    expect(parser.parse(program)).toStrictEqual({
      type: 'Program',
      body: [
        {
          type: 'ExpressionStatement',
          expression: {
            type: 'AssignmentExpression',
            operator: '=',
            left: {
              type: 'Identifier',
              name: 'x'
            },
            right: {
              type: 'AssignmentExpression',
              operator: '=',
              left: {
                type: 'Identifier',
                name: 'y'
              },
              right: {
                type: 'NumericLiteral',
                value: 42
              }
            }
          }
        }
      ]
    });
  });

  test('should throw on invalid assignments', () => {
    const program = '42 = 42;';
    expect(() => parser.parse(program)).toThrowError(SyntaxError);
    expect(() => parser.parse(program)).toThrow('Invalid left-hand side in assignment expression.');
  });

  test('should correctly parse arithmetic using identifiers', () => {
    expect(parser.parse('x + x;')).toStrictEqual({
      type: 'Program',
      body: [
        {
          type: 'ExpressionStatement',
          expression: {
            type: 'BinaryExpression',
            operator: '+',
            left: {
              type: 'Identifier',
              name: 'x'
            },
            right: {
              type: 'Identifier',
              name: 'x'
            }
          }
        }
      ]
    });


    expect(parser.parse('x = y + 10;')).toStrictEqual({
      type: 'Program',
      body: [
        {
          type: 'ExpressionStatement',
          expression: {
            type: 'AssignmentExpression',
            operator: '=',
            left: {
              type: 'Identifier',
              name: 'x'
            },
            right: {
              type: 'BinaryExpression',
              operator: '+',
              left: {
                type: 'Identifier',
                name: 'y'
              },
              right: {
                type: 'NumericLiteral',
                value: 10
              }
            }
          }
        }
      ]
    });
  });

  test('Should parse complex assignments', () => {
    expect(parser.parse('x += 10;')).toStrictEqual({
      type: 'Program',
      body: [
        {
          type: 'ExpressionStatement',
          expression: {
            type: 'AssignmentExpression',
            operator: '+=',
            left: {
              type: 'Identifier',
              name: 'x'
            },
            right: {
              type: 'NumericLiteral',
              value: 10
            }
          }
        }
      ]
    });
  });
});
