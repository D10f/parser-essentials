import { describe, test, expect, beforeEach } from 'vitest';
import { Parser } from '../Parser';

describe('Test suite for simple literal expressions', () => {
  let parser: Parser;

  beforeEach(() => {
    parser = new Parser();
  });

  test('should return a number', () => {
    const program = parser.parse('42;');

    expect(program).toStrictEqual({
      type: 'Program',
      body: [
        {
          type: 'ExpressionStatement',
          expression: {
            type: 'NumericLiteral',
            value: 42,
          },
        },
      ],
    });
  });

  test('should parse a string /w double quotes', () => {
    const program = parser.parse('"hello world";');

    expect(program).toStrictEqual({
      type: 'Program',
      body: [
        {
          type: 'ExpressionStatement',
          expression: {
            type: 'StringLiteral',
            value: 'hello world',
          },
        },
      ],
    });
  });

  test('should parse a string /w single quotes', () => {
    const program = parser.parse("'hello world';");

    expect(program).toStrictEqual({
      type: 'Program',
      body: [
        {
          type: 'ExpressionStatement',
          expression: {
            type: 'StringLiteral',
            value: 'hello world',
          },
        },
      ],
    });
  });

  test('should ignore whitespaces', () => {
    expect(parser.parse('    42   ;')).toStrictEqual({
      type: 'Program',
      body: [
        {
          type: 'ExpressionStatement',
          expression: {
            type: 'NumericLiteral',
            value: 42,
          },
        },
      ],
    });

    expect(parser.parse(" 'hello world'  ;")).toStrictEqual({
      type: 'Program',
      body: [
        {
          type: 'ExpressionStatement',
          expression: {
            type: 'StringLiteral',
            value: 'hello world',
          },
        },
      ],
    });
  });

  test('should ignore single-line comments', () => {
    const program = parser.parse(`
      // Number:
      42;
    `);

    expect(program).toStrictEqual({
      type: 'Program',
      body: [
        {
          type: 'ExpressionStatement',
          expression: {
            type: 'NumericLiteral',
            value: 42,
          },
        },
      ],
    });
  });

  test('should ignore multi-line comments', () => {
    const program = parser.parse(`
      /** Number:
       * this is a comment
       */
      42;
    `);

    expect(program).toStrictEqual({
      type: 'Program',
      body: [
        {
          type: 'ExpressionStatement',
          expression: {
            type: 'NumericLiteral',
            value: 42,
          },
        },
      ],
    });
  });

});
