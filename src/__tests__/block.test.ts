import { describe, test, expect, beforeEach } from 'vitest';
import { Parser } from '../Parser';

describe('Test suite for block statements', () => {

  const parser = new Parser();

  test('Should return a block statement', () => {
    const program = parser.parse(`
      {
        42;
        "hello";
      }
    `);

    expect(program).toStrictEqual({
      type: 'Program',
      body: [
        {
          type: 'BlockStatement',
          body: [
            {
              type: 'ExpressionStatement',
              expression: {
                type: 'NumericLiteral',
                value: 42
              },
            },
            {
              type: 'ExpressionStatement',
              expression: {
                type: 'StringLiteral',
                value: 'hello'
              }
            }
          ]
        },
      ]
    });
  });

  test('should parse an empty block', () => {
    const program = parser.parse(`{}`);
    expect(program).toStrictEqual({
      type: 'Program',
      body: [
        {
          type: 'BlockStatement',
          body: []
        }
      ]
    });
  });

  test('should parse nested blocks', () => {
    const program = parser.parse(`
      {
        42;
        {
          "hello";
        }
      }
    `);
    expect(program).toStrictEqual({
      type: 'Program',
      body: [
        {
          type: 'BlockStatement',
          body: [
            {
              type: 'ExpressionStatement',
              expression: {
                type: 'NumericLiteral',
                value: 42
              }
            },
            {
              type: 'BlockStatement',
              body: [
                {
                  type: 'ExpressionStatement',
                  expression: {
                    type: 'StringLiteral',
                    value: 'hello'
                  }
                }
              ]
            }
          ]
        }
      ]
    })
  });
});
