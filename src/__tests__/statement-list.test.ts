import { describe, test, expect, beforeEach } from 'vitest';
import { Parser } from '../Parser';

describe('Test suite for statement expressions', () => {

  const parser = new Parser();

  test('should parse a program with mutliple statements', () => {
    const program = parser.parse(`"Hello, world"; 42;`);
    expect(program).toStrictEqual({
      type: 'Program',
      body: [
        {
          type: 'ExpressionStatement',
          expression: {
            type: 'StringLiteral',
            value: 'Hello, world',
          },
        },
        {
          type: 'ExpressionStatement',
          expression: {
            type: 'NumericLiteral',
            value: 42,
          },
        },
      ],
    });
  });

  test('should handle empty statements', () => {
    expect(parser.parse(`;`)).toStrictEqual({
      type: 'Program',
      body: [
        {
          type: 'EmptyStatement'
        }
      ]
    });
  });
});
