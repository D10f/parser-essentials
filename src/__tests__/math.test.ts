import { describe, test, expect } from 'vitest';
import { Parser } from '../Parser';

describe('Test suite for math expressions (binary expressions)', () => {
  const parser = new Parser();

  test('Should parse a binary expression adding two numbers', () => {
    const program = parser.parse('2 + 2;');

    expect(program).toStrictEqual({
      type: 'Program',
      body: [
        {
          type: 'ExpressionStatement',
          expression: {
            type: 'BinaryExpression',
            operator: '+',
            left: {
              type: 'NumericLiteral',
              value: 2,
            },
            right: {
              type: 'NumericLiteral',
              value: 2,
            },
          },
        },
      ],
    });
  });

  test('Should parse a binary expression adding multiple numbers', () => {
    const program = parser.parse('3 + 2 - 2;');

    expect(program).toStrictEqual({
      type: 'Program',
      body: [
        {
          type: 'ExpressionStatement',
          expression: {
            type: 'BinaryExpression',
            operator: '-',
            left: {
              type: 'BinaryExpression',
              operator: '+',
              left: {
                type: 'NumericLiteral',
                value: 3,
              },
              right: {
                type: 'NumericLiteral',
                value: 2,
              },
            },
            right: {
              type: 'NumericLiteral',
              value: 2,
            },
          },
        },
      ],
    });
  });

  test('Should parse a binary expression multiplicating two numbers', () => {
    const program = parser.parse('4 * 4;');

    expect(program).toStrictEqual({
      type: 'Program',
      body: [
        {
          type: 'ExpressionStatement',
          expression: {
            type: 'BinaryExpression',
            operator: '*',
            left: {
              type: 'NumericLiteral',
              value: 4,
            },
            right: {
              type: 'NumericLiteral',
              value: 4,
            },
          },
        },
      ],
    });
  });

  test('Should parse a binary expression multiplicating two numbers', () => {
    const program = parser.parse('2 + 4 * 4;');

    expect(program).toStrictEqual({
      type: 'Program',
      body: [
        {
          type: 'ExpressionStatement',
          expression: {
            type: 'BinaryExpression',
            operator: '+',
            left: {
              type: 'NumericLiteral',
              value: 2,
            },
            right: {
              type: 'BinaryExpression',
              operator: '*',
              left: {
                type: 'NumericLiteral',
                value: 4,
              },
              right: {
                type: 'NumericLiteral',
                value: 4,
              },
            },
          },
        },
      ],
    });
  });

  test('Should parse a binary expression multiplicating two numbers', () => {
    const program = parser.parse('(2 + 4) * 4;');

    expect(program).toStrictEqual({
      type: 'Program',
      body: [
        {
          type: 'ExpressionStatement',
          expression: {
            type: 'BinaryExpression',
            operator: '*',
            left: {
              type: 'BinaryExpression',
              operator: '+',
              left: {
                type: 'NumericLiteral',
                value: 2,
              },
              right: {
                type: 'NumericLiteral',
                value: 4,
              },
            },
            right: {
              type: 'NumericLiteral',
              value: 4,
            },
          },
        },
      ],
    });
  });
});
