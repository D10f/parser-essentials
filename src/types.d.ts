type LiteralToken = 'STRING' | 'NUMBER';
type MathToken = 'ADDITIVE_OPERATOR' | 'MULTIPLICATIVE_OPERATOR';
type IdentifierToken = 'IDENTIFIER';
type DelimiterToken = ';';
type BlockToken = '{' | '}' | '(' | ')';
type AssignmentToken = 'SIMPLE_ASSIGN' | 'COMPLEX_ASSIGN';

type TokenType =
  | LiteralToken
  | IdentifierToken
  | DelimiterToken
  | BlockToken
  | MathToken
  | AssignmentToken;

type MathOperator = '+' | '-' | '*' | '/';

type Token = {
  type: TokenType;
  value: string | number;
};

type EmptyStatement = {
  type: 'EmptyStatement';
};

type BlockStatement = {
  type: 'BlockStatement';
  body: StatementList;
};

type ExpressionStatement = {
  type: 'ExpressionStatement';
  expression: MultiplicativeExpression | PrimaryExpression;
};

type NumericLiteral = {
  type: 'NumericLiteral';
  value: number;
};

type StringLiteral = {
  type: 'StringLiteral';
  value: string;
};

type LiteralExp = NumericLiteral | StringLiteral;
type PrimaryExpression = NumericLiteral | StringLiteral;

// ------------

type BinaryExpression = {
  type: 'BinaryExpression';
  operator: MathOperator;
  left: AdditiveExpression | MultiplicativeExpression;
  right: AdditiveExpression | MultiplicativeExpression;
};

type AdditiveExpression = BinaryExpression | PrimaryExpression;

type MultiplicativeExpression = BinaryExpression | PrimaryExpression;

// type AdditiveExpression = {
//   type: 'BinaryExpression';
//   operator: MathOperator;
//   left: MultiplicativeExpression;
//   right: MultiplicativeExpression;
// };

// type MultiplicativeExpression = {
//   type: 'BinaryExpression';
//   operator: MathOperator;
//   left: MultiplicativeExpression | PrimaryExpression;
//   right: MultiplicativeExpression | PrimaryExpression;
// };

interface AST {
  type: 'Program';
  body: StatementList;
}

type StatementList = Statement[] | Statement;

type Statement = ExpressionStatement | BlockStatement | EmptyStatement;

/**
 * Returns a type that is the union of all keys corresponding to methods in a type.
 */
type MethodOf<T> = keyof {
  [Key in keyof T as T[Key] extends Function ? Key : never]: T[Key];
};
