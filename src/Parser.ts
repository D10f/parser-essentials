import { Tokenizer } from './Tokenizer';

/**
 * Letter parser: recursive descent implementation.
 */
export class Parser {
  private string: string;
  private tokenizer: Tokenizer;
  private lookahead: Token | null;

  constructor() {
    this.string = '';
    this.tokenizer = new Tokenizer();
    this.lookahead = null;
  }

  /**
   * Parses a string into an AST.
   */
  parse(input: string) {
    this.string = input;
    this.tokenizer.init(this.string);

    // Obtain the first token, used for predictive parsing.
    this.lookahead = this.tokenizer.next();

    return this.Program();
  }

  /**
   * Main entry point
   *
   * Program
   *  : StatementList
   *  ;
   */
  Program() {
    return {
      type: 'Program',
      body: this.StatementList(),
    };
  }

  /**
   * StatementList
   *  : Statement
   *  | StatementList Statement -> Statement Statement ...
   *  ;
   */
  StatementList(
    stopLookaheadSymbol: Extract<BlockToken, '}' | ')'> | null = null
  ) {
    const statementList: StatementList = [];

    while (this.lookahead && this.lookahead.type !== stopLookaheadSymbol) {
      statementList.push(this.Statement());
    }

    return statementList;
  }

  /**
   * Statement
   *  : ExpressionStatement
   *  | BlockStatement
   *  | EmptyStatement
   *  ;
   */
  Statement(): EmptyStatement | BlockStatement | ExpressionStatement {
    switch (this.lookahead!.type) {
      case ';':
        return this.EmptyStatement();
      case '{':
        return this.BlockStatement();
      default:
        return this.ExpressionStatement();
    }
  }

  /**
   * EmptyStatement
   *  : ';'
   *  ;
   */
  EmptyStatement(): EmptyStatement {
    this.eat(';');
    return {
      type: 'EmptyStatement',
    };
  }

  /**
   * Block Statement
   *  : '{' OptionalStatementList '}'
   *  ;
   */
  BlockStatement(): BlockStatement {
    this.eat('{');

    const body = this.StatementList('}');

    this.eat('}');

    return {
      type: 'BlockStatement',
      body,
    };
  }

  /**
   * ExpressionStatement
   *  : Expression ';'
   */
  ExpressionStatement(): ExpressionStatement {
    const expression = this.Expression();

    this.eat(';');

    return {
      type: 'ExpressionStatement',
      expression,
    };
  }

  /**
   * Expression
   *  : Literal
   *  ;
   */
  Expression() {
    return this.AssignmentExpression();
  }

  /**
   * AssignmentExpression
   *  : AdditiveExpression
   *  | LeftHandSideExpression AssignmentOperator AssignmentExpression
   *  ;
   */
  AssignmentExpression(): any {
    const left = this.AdditiveExpression();

    if (!this.isAssignmentOperator(this.lookahead!.type)) {
      return left;
    }

    return {
      type: 'AssignmentExpression',
      operator: this.AssignmentOperator().value,
      left: this.checkValidAssignmentTarget(left),
      right: this.AssignmentExpression(),
    };
  }

  /**
   * LeftHandSideExpression
   *  : Identifier
   *  ;
   */
  LeftHandSideExpression() {
    return this.Identifier();
  }

  /**
   * Identifier
   *  : Identifier
   *  ;
   */
  Identifier() {
    const { value } = this.eat('IDENTIFIER');
    return {
      type: 'Identifier',
      name: value
    };
  }

  /**
   * Whether the node is a valid assignment target.
   */
  checkValidAssignmentTarget(node: any) {
    if (node.type === 'Identifier') {
      return node;
    }

    throw new SyntaxError('Invalid left-hand side in assignment expression.');
  }

  /**
   * Whether the token is an assignment operator.
   */
  isAssignmentOperator(tokenType: TokenType) {
    return tokenType === 'SIMPLE_ASSIGN' || tokenType === 'COMPLEX_ASSIGN';
  }

  /**
   * AssignmentOperator
   *  : SIMPLE_ASSIGN
   *  | COMPLEX_ASSIGN
   *  ;
   */
  AssignmentOperator() {
    return this.lookahead!.type === 'SIMPLE_ASSIGN'
      ? this.eat('SIMPLE_ASSIGN')
      : this.eat('COMPLEX_ASSIGN');
  }

  /**
   * Generic binary expression
   */
  BinaryExpression(
    builderName: 'PrimaryExpression' | 'MultiplicativeExpression',
    operatorToken: MathToken
  ): BinaryExpression {
    let left = this[builderName]();

    while (this.lookahead!.type === operatorToken) {
      const operator = this.eat(operatorToken).value;
      //const right = this[builderName]();
      const right = this[builderName]();

      left = {
        type: 'BinaryExpression',
        operator,
        left,
        right,
      };
    }

    return left;
  }

  /**
   * AdditiveExpression
   *  : MultiplicativeExpression
   *  | AdditiveExpression ADDITIVE_OPERATOR MultiplicativeExpression -> MultiplicativeExpression ADDITIVE_OPERATOR MultiplicativeExpression ...
   *  ;
   */
  AdditiveExpression(): AdditiveExpression {
    return this.BinaryExpression(
      'MultiplicativeExpression',
      'ADDITIVE_OPERATOR'
    );
  }

  /**
   * MultiplicativeExpression
   *  : MultiplicativeExpression
   *  | MultiplicativeExpression MULTIPLICATIVE_OPERATOR MultiplicativeExpression -> MultiplicativeExpression MULTIPLICATIVE_OPERATOR MultiplicativeExpression ...
   *  ;
   */
  MultiplicativeExpression(): MultiplicativeExpression {
    return this.BinaryExpression(
      'PrimaryExpression',
      'MULTIPLICATIVE_OPERATOR'
    );
  }

  /**
   * PrimaryExpression
   *  : Literal
   *  | ParenthesizedExpression
   *  | LeftHandSideExpression
   *  ;
   */
  PrimaryExpression(): any {
    switch (this.lookahead!.type) {
      case 'STRING':
      case 'NUMBER':
        return this.Literal();
      case '(':
        return this.ParenthesizedExpression();
      default:
        return this.LeftHandSideExpression();
    }
  }

  /**
   * ParenthesizedExpression
   *  : '(' Expression ')'
   */
  ParenthesizedExpression(): any {
    this.eat('(');
    const expression = this.Expression();
    this.eat(')');
    return expression;
  }

  /**
   * Literal
   *  : NumericLiteral
   *  | StringLiteral
   *  ;
   */
  Literal(): any {
    if (this.lookahead === null) {
      throw new SyntaxError('Literal: unexpected literal production');
    }

    switch (this.lookahead.type) {
      case 'NUMBER':
        return this.NumericLiteral();
      case 'STRING':
        return this.StringLiteral();
      default:
        throw new SyntaxError(`Unexpected token: "${this.lookahead.value}".`);
    }
  }

  /**
   * NumericLiteral
   *  : NUMBER
   *  ;
   */
  NumericLiteral(): NumericLiteral {
    const token = this.eat('NUMBER');

    return {
      type: 'NumericLiteral',
      value: Number(token.value),
    };
  }

  /**
   * StringLiteral
   *  : STRING
   *  ;
   */
  StringLiteral(): StringLiteral {
    const token = this.eat('STRING');

    return {
      type: 'StringLiteral',
      value: (token.value as string).slice(1, -1), // remove the quotes
    };
  }

  /**
   * Expects a token of a given type
   */
  eat(tokenType: TokenType): Token {
    const token = this.lookahead;

    if (token === null) {
      throw new SyntaxError(
        `Unexpected end of input, expected: "${tokenType}"`
      );
    }

    if (token.type !== tokenType) {
      throw new SyntaxError(
        `Unexpected token: "${token.value}", expected: "${tokenType}"`
      );
    }

    // Advance to the next token
    this.lookahead = this.tokenizer.next();

    return token;
  }
}
