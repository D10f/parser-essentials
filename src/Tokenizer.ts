/**
 * Tokenizer specification
 */
const spec: [RegExp, TokenType | null][] = [
  // whitespaces
  [new RegExp(/^\s+/), null],

  // single- and multi-line comments
  [new RegExp(/^\/\/.*/), null],
  [new RegExp(/^\/\*[\s\S]*?\*\//), null],

  // symbols, delimiters
  [new RegExp(/^;/), ';'],
  [new RegExp(/^\{/), '{'],
  [new RegExp(/^\}/), '}'],
  [new RegExp(/^\(/), '('],
  [new RegExp(/^\)/), ')'],

  // number literals
  [new RegExp(/^\d+/), 'NUMBER'],

  // single- and double-quoted string literals
  [new RegExp(/"[^"]*"/), 'STRING'],
  [new RegExp(/'[^']*'/), 'STRING'],

  // Identifiers:
  [new RegExp(/^\w+/), 'IDENTIFIER'],

  // Assignment operators: =, +=, -=, *=, /=
  [new RegExp(/^=/), 'SIMPLE_ASSIGN'],
  [new RegExp(/^[\*\/\+\-]=/), 'COMPLEX_ASSIGN'],

  // Math operators: + -
  [new RegExp(/^[+\-]/), 'ADDITIVE_OPERATOR'],
  [new RegExp(/^[\*\/]/), 'MULTIPLICATIVE_OPERATOR'],

];

/**
 * Tokenizer class
 *
 * Lazily pulls a token from a stream
 */
export class Tokenizer {
  private string = '';
  private cursor = 0;

  /**
   * Initializes the input string.
   */
  init(input: string) {
    this.string = input;
    this.cursor = 0;
  }

  /**
   * Checks whether ths tokenizer has consumed the input string.
   */
  isEOF() {
    return this.cursor === this.string.length;
  }

  /**
   * Checks whether the input string has been consumed fully.
   */
  hasMoreTokens() {
    return this.cursor < this.string.length;
  }

  /**
   * Retrieves the next token.
   */
  next(): Token | null {
    if (!this.hasMoreTokens()) {
      return null;
    }

    const str = this.string.slice(this.cursor);

    for (const [re, tokenType] of spec) {
      const tokenValue = this.match(re, str);

      if (tokenValue === null) {
        continue;
      }

      // If token type should be ignored eg whitespaces, ignore and continue
      // retrieving tokens
      if (tokenType === null) {
        return this.next();
      }

      return {
        type: tokenType,
        value: tokenValue,
      };
    }

    throw new SyntaxError(`Unexpected token: "${str[0]}"`);
  }

  /**
   * Matches a token for a regular expression
   */
  match(re: RegExp, str: string): string | number | null {
    const matched = re.exec(str);

    if (matched == null) {
      return null;
    }

    this.cursor += matched[0].length;
    return matched[0];
  }
}
